import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-alimentos',
  templateUrl: './alimentos.component.html',
  styleUrls: ['./alimentos.component.css']
})
export class AlimentosComponent implements OnInit {

  constructor(private route:Router) { }
  
  Cafe(){
    this.route.navigate(['cafe']);
  }
  Pastas(){
    this.route.navigate(['pastas']);
  }
  Aceites(){
    this.route.navigate(['aceites']);
  }
  Gomitas(){
    this.route.navigate(['gomitas']);
  }
  Enlatados(){
    this.route.navigate(['enlatados']);
  }
  ngOnInit(): void {
  }

}
