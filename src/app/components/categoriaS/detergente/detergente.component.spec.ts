import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetergenteComponent } from './detergente.component';

describe('DetergenteComponent', () => {
  let component: DetergenteComponent;
  let fixture: ComponentFixture<DetergenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetergenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetergenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
