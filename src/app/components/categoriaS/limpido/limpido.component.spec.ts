import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LimpidoComponent } from './limpido.component';

describe('LimpidoComponent', () => {
  let component: LimpidoComponent;
  let fixture: ComponentFixture<LimpidoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LimpidoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LimpidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
