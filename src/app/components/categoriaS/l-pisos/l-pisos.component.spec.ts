import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LPisosComponent } from './l-pisos.component';

describe('LPisosComponent', () => {
  let component: LPisosComponent;
  let fixture: ComponentFixture<LPisosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LPisosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LPisosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
