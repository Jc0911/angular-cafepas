import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aseo',
  templateUrl: './aseo.component.html',
  styleUrls: ['./aseo.component.css']
})
export class AseoComponent implements OnInit {

  constructor(private route:Router) { }
  
  Detergente(){
    this.route.navigate(['detergente']);
  }
  LimpiaPisos(){
    this.route.navigate(['l-pisos']);
  }
  Limpido(){
    this.route.navigate(['limpido']);
  }
  ngOnInit(): void {
  }

}
