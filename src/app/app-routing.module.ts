import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InicioComponent} from './components/inicio/inicio.component'; 
import { AlimentosComponent } from './components/alimentos/alimentos.component';
import { AseoComponent } from './components/aseo/aseo.component';
import { AceiteComponent } from './components/categoriaA/aceite/aceite.component';
import { CafeComponent } from './components/categoriaA/cafe/cafe.component';
import { EnlatadosComponent } from './components/categoriaA/enlatados/enlatados.component';
import { GomitasComponent } from './components/categoriaA/gomitas/gomitas.component';
import { PastasComponent } from './components/categoriaA/pastas/pastas.component';
import { DetergenteComponent } from './components/categoriaS/detergente/detergente.component';
import { LPisosComponent } from './components/categoriaS/l-pisos/l-pisos.component';
import { LimpidoComponent } from './components/categoriaS/limpido/limpido.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { RecetasComponent } from './components/recetas/recetas.component';
import { SobrenosotrosComponent } from './components/sobrenosotros/sobrenosotros.component';

const routes: Routes = [
  {path: '', redirectTo: 'inicio', pathMatch: 'full'},
  {path: 'inicio', component: InicioComponent},
  {path: 'sobrenosotros', component: SobrenosotrosComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: 'alimentos', component: AlimentosComponent},
  {path: 'recetas', component: RecetasComponent},
  {path: 'aseo', component: AseoComponent},
  {path: 'cafe', component: CafeComponent},
  {path: 'pastas', component: PastasComponent},
  {path: 'aceites', component: AceiteComponent},
  {path: 'gomitas', component: GomitasComponent},
  {path: 'enlatados', component: EnlatadosComponent},
  {path: 'detergente', component: DetergenteComponent},
  {path: 'l-pisos', component: LPisosComponent},
  {path: 'limpido', component: LimpidoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
