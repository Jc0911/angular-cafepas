import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { SobrenosotrosComponent } from './components/sobrenosotros/sobrenosotros.component';
import { AlimentosComponent } from './components/alimentos/alimentos.component';
import { AseoComponent } from './components/aseo/aseo.component';
import { ContactoComponent } from './components/contacto/contacto.component';
import { RecetasComponent } from './components/recetas/recetas.component';
import { CafeComponent } from './components/categoriaA/cafe/cafe.component';
import { PastasComponent } from './components/categoriaA/pastas/pastas.component';
import { AceiteComponent } from './components/categoriaA/aceite/aceite.component';
import { GomitasComponent } from './components/categoriaA/gomitas/gomitas.component';
import { EnlatadosComponent } from './components/categoriaA/enlatados/enlatados.component';
import { DetergenteComponent } from './components/categoriaS/detergente/detergente.component';
import { LPisosComponent } from './components/categoriaS/l-pisos/l-pisos.component';
import { LimpidoComponent } from './components/categoriaS/limpido/limpido.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    SobrenosotrosComponent,
    AlimentosComponent,
    AseoComponent,
    ContactoComponent,
    RecetasComponent,
    CafeComponent,
    PastasComponent,
    AceiteComponent,
    GomitasComponent,
    EnlatadosComponent,
    DetergenteComponent,
    LPisosComponent,
    LimpidoComponent,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
