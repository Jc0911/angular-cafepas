import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'p-cafepas';
  constructor(private route:Router){}
    
  Alimentos(){
    this.route.navigate(['alimentos']);
}
  Aseo(){
    this.route.navigate(['aseo']);
  }

  SobreNosotros(){
    this.route.navigate(['sobrenosotros']);
  }
  Recetas(){
    this.route.navigate(['recetas']);
  }
  Contacto(){
    this.route.navigate(['contacto']);
  }

}
